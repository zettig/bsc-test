package bsc.msc.test;

import java.util.List;

import bsc.msc.test.datamodel.BaseAccount;

public interface AccountRepository {
    List<BaseAccount> accounts();
}
