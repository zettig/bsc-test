package bsc.msc.test;

import java.util.List;

import bsc.msc.test.datamodel.BaseAccount;
import bsc.msc.test.mvp.MvpPresenter;
import bsc.msc.test.mvp.MvpView;

public interface AccountListContract {
    interface AccountListView extends MvpView {
        void showAccountList(List<BaseAccount> accountList);
//        void showProgress();
//        void hideProgress();
//        void showError();
    }

    interface AccountListPresenter extends MvpPresenter<AccountListView> {

    }
}
