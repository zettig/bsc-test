package bsc.msc.test;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import bsc.msc.test.datamodel.BaseAccount;

public class MainActivity extends AppCompatActivity implements AccountListContract.AccountListView {

    private RecyclerView recyclerView;
    private AccountListContract.AccountListPresenter presenter;
    private AccountListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new AccountListPresenter(new AccountRepositoryImpl());
        presenter.attachView(this);

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new AccountListAdapter();
        recyclerView.setAdapter(adapter);

        presenter.viewIsReady();
    }

    @Override
    public void showAccountList(List<BaseAccount> accountList) {
        adapter.setmList(accountList);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
        presenter.destroy();
    }
}
