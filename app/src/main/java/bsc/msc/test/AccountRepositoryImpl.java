package bsc.msc.test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import bsc.msc.test.datamodel.BaseAccount;
import bsc.msc.test.datamodel.EurAccount;
import bsc.msc.test.datamodel.RubAccount;
import bsc.msc.test.datamodel.UsdAccount;

public class AccountRepositoryImpl implements AccountRepository {
    @Override
    public List<BaseAccount> accounts() {
        List<BaseAccount> accounts = new ArrayList<>();
        accounts.add(new EurAccount("EUR", new BigDecimal(123)));
        accounts.add(new UsdAccount("USD", new BigDecimal(444)));
        accounts.add(new RubAccount("RUB", new BigDecimal(1999)));
        return accounts;
    }
}
