package bsc.msc.test.datamodel;

public enum Concurrency {
    RUB(0), USD(1), EUR(2);
    int order;

    Concurrency(int order) {
        this.order = order;
    }

    public int getOrder() {
        return order;
    }
}
