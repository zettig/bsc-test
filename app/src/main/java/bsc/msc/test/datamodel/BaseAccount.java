package bsc.msc.test.datamodel;

import java.math.BigDecimal;

public abstract class BaseAccount {
    private String title;
    private BigDecimal amount;

    public BaseAccount(String title, BigDecimal amount) {
        this.title = title;
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public abstract Concurrency getConcurrencyType();
    public abstract int getLayout();
}
