package bsc.msc.test.datamodel;

import java.math.BigDecimal;

import bsc.msc.test.R;

public class EurAccount extends BaseAccount {
    public EurAccount(String title, BigDecimal amount) {
        super(title, amount);
    }

    @Override
    public Concurrency getConcurrencyType() {
        return Concurrency.EUR;
    }

    @Override
    public int getLayout() {
        return R.layout.item_account_eur;
    }
}
