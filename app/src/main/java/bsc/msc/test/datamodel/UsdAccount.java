package bsc.msc.test.datamodel;

import java.math.BigDecimal;

import bsc.msc.test.R;

public class UsdAccount extends BaseAccount {
    public UsdAccount(String title, BigDecimal amount) {
        super(title, amount);
    }

    @Override
    public Concurrency getConcurrencyType() {
        return Concurrency.USD;
    }

    @Override
    public int getLayout() {
        return R.layout.item_account_usd;
    }
}
