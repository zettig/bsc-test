package bsc.msc.test.datamodel;

import java.math.BigDecimal;

import bsc.msc.test.R;

public class RubAccount extends BaseAccount {
    public RubAccount(String title, BigDecimal amount) {
        super(title, amount);
    }

    @Override
    public Concurrency getConcurrencyType() {
        return Concurrency.RUB;
    }

    @Override
    public int getLayout() {
        return R.layout.item_account_rub;
    }
}
