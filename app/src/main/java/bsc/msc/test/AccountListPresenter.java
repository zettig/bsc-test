package bsc.msc.test;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import bsc.msc.test.datamodel.BaseAccount;
import bsc.msc.test.mvp.BasePresenter;

public class AccountListPresenter extends BasePresenter<AccountListContract.AccountListView> implements AccountListContract.AccountListPresenter {
    private AccountRepository repository;

    AccountListPresenter(AccountRepository repository) {
        this.repository = repository;
    }

    @Override
    public void viewIsReady() {
        List<BaseAccount> accounts = repository.accounts();
        Collections.sort(accounts, new AccountComparator());
        view.showAccountList(accounts);
    }

    private class AccountComparator implements Comparator<BaseAccount> {
        @Override
        public int compare(BaseAccount o1, BaseAccount o2) {
            return Integer.compare(o1.getConcurrencyType().getOrder(), o2.getConcurrencyType().getOrder());
        }
    }
}
