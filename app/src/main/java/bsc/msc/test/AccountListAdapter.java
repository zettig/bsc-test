package bsc.msc.test;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.NoSuchElementException;

import bsc.msc.test.datamodel.BaseAccount;
import bsc.msc.test.viewholder.BaseViewHolder;
import bsc.msc.test.viewholder.EurAccountViewHolder;
import bsc.msc.test.viewholder.RubAccountViewHolder;
import bsc.msc.test.viewholder.UsdAccountViewHolder;

public class AccountListAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private List<BaseAccount> mList;

    public void setmList(List<BaseAccount> accountList) {
        this.mList = accountList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return mList.get(position).getLayout();
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(viewType, viewGroup, false);
        switch (viewType) {
            case R.layout.item_account_rub:
                return new RubAccountViewHolder(view);
            case R.layout.item_account_usd:
                return new UsdAccountViewHolder(view);
            case R.layout.item_account_eur:
                return new EurAccountViewHolder(view);
            default: return null;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void onBindViewHolder(@NonNull BaseViewHolder baseViewHolder, int position) {
        baseViewHolder.bind(mList.get(position));
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }
}
