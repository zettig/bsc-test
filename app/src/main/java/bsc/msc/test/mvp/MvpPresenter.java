package bsc.msc.test.mvp;

public interface MvpPresenter<V extends MvpView> {
    void attachView(V mvpView);
    void detachView();
    void viewIsReady();
    void destroy();
}
