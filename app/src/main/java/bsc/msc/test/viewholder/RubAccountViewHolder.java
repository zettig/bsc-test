package bsc.msc.test.viewholder;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import bsc.msc.test.R;
import bsc.msc.test.datamodel.RubAccount;

public class RubAccountViewHolder extends BaseViewHolder<RubAccount> {
    private TextView title;
    private TextView amount;
    private ImageView img;

    public RubAccountViewHolder(@NonNull View itemView) {
        super(itemView);
        title = itemView.findViewById(R.id.item_account_title);
        amount = itemView.findViewById(R.id.item_account_amount);
        img = itemView.findViewById(R.id.item_account_img);
    }

    @Override
    public void bind(RubAccount item) {
        title.setText(item.getTitle());
        amount.setText(item.getAmount().toPlainString());
        img.setImageResource(R.drawable.ic_rub);
    }
}
